#!/bin/bash

# Set file locations. Relative to project root
CHART_YAML_FILE=./deployment/helm/Chart.yaml
INCREMENT_VERSION_SCRIPT=./deployment/scripts/increment_version.sh

# Check if version has been already touched or not
if [ "$(git diff --exit-code HEAD -- ${CHART_YAML_FILE})" ]
then
  echo "Chart.yaml was already changed. Skipping auto-incrementation of appVersion."
  exit 0
fi

# Parse the current version and bump it one patch level
CURRENT_APP_VERSION=$(grep "appVersion:" "${CHART_YAML_FILE}" | cut -d' ' -f2)
NEW_APP_VERSION=$("${INCREMENT_VERSION_SCRIPT}" -p "${CURRENT_APP_VERSION}")

# Edit and stage the file
sed -i "s/appVersion: ${CURRENT_APP_VERSION}/appVersion: ${NEW_APP_VERSION}/" "${CHART_YAML_FILE}"
git add "${CHART_YAML_FILE}"
echo "Chart.yaml was not changed, but the code was. Running auto-incrementation of appVersion."
