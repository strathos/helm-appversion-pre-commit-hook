# Git pre-commit hook to auto-increment Helm appVersion
Auto-increments appVersion in Chart.yaml if the file was not touched manually.

This is a client-side Git hook, so it needs to be enabled on each client separately
```bash
cp deployment/scripts/pre-commit.sh .git/hooks/pre-commit
```